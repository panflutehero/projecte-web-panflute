//NOTES A TOCAR EN FORMA DE VARIABLE
var do1 = document.getElementById("notaDo");
var re1 = document.getElementById("notaRe");
var mi1 = document.getElementById("notaMi");
var fa1 = document.getElementById("notaFa");
var sol1 = document.getElementById("notaSol");
var la1 = document.getElementById("notaLa");
var si1 = document.getElementById("notaSi");
var doA1 = document.getElementById("notaDoAgut");

//VARIABLES GLOBALS
var puntuacio = 0;
var puntuaciocanviada = true;
var comencar = false;
var array;
var posicio = -1;
var bucle;
var gegant = ["Fa", "Sol", "La", "Si", "Do", "Sol", "La", "Si", "Do", "Re", "Do", "Re", "Mi", "Fa", "Sol"];
var nadal = ["Fa", "Sol", "La", "Do", "Re", "Mi", "Sol", "La", "Si", "Re", "Mi", "Fa", "Si", "Do", "Re"];

//FUNCIO QUE PUNTUA UN COP S'HA APRETAT EL BOTO "COMENÇAR"
function reproduirso(id) {
    if (id == 1) {
        do1.play();
        if (comencar == true) {
            if (array[posicio] == 'Do') {
                puntuaciobe();
            } else {
                puntuaciomalament();
            }
        }
    } else if (id == 2) {
        re1.play();
        if (comencar == true) {
            if (array[posicio] == 'Re') {
                puntuaciobe();
            } else {
				puntuaciomalament();
            }
        }
    } else if (id == 3) {
        mi1.play();
        if (comencar == true) {
            if (array[posicio] == 'Mi') {
                puntuaciobe();
            } else {
				puntuaciomalament();
            }
        }
    } else if (id == 4) {
        fa1.play();
        if (comencar == true) {
            if (array[posicio] == 'Fa') {
                puntuaciobe();
            } else {
				puntuaciomalament();
            }
        }
    } else if (id == 5) {
        sol1.play();
        if (comencar == true) {
            if (array[posicio] == 'Sol') {
                puntuaciobe();
            } else {
				puntuaciomalament();
            }
        }
    } else if (id == 6) {
        la1.play();
        if (comencar == true) {
            if (array[posicio] == 'La') {
                puntuaciobe();
            } else {
				puntuaciomalament();
            }
        }
    } else if (id == 7) {
        si1.play();
        if (comencar == true) {
            if (array[posicio] == 'Si') {
                puntuaciobe();
            } else {
				puntuaciomalament();
            }
        }
    } else if (id == 8) {
        doA1.play();
        if (comencar == true) {
            if (array[posicio] == 'DoA') {
                puntuaciobe();
            } else {
				puntuaciomalament();
            }
        }
    }
}

//EVENT LISTENER QUE PERMET TOCAR L'INSTRUMENT AMB ELS NUMEROS 1-8 DEL TECLAT
window.addEventListener("keypress", colorTecla, false)
window.addEventListener("keyup", tornarTecla, false);

//TECLA APRETADA
function colorTecla(tecla) {
    if (tecla.keyCode == "49") {
        reproduirso("do");
        document.getElementById("do").className = "moure";
    } else if (tecla.keyCode == "50") {
        reproduirso("re");
        document.getElementById("re").className = "moure";
    } else if (tecla.keyCode == "51") {
        reproduirso("mi")
        document.getElementById("mi").className = "moure";
    } else if (tecla.keyCode == "52") {
        reproduirso("fa")
        document.getElementById("fa").className = "moure";
    } else if (tecla.keyCode == "53") {
        reproduirso("sol")
        document.getElementById("sol").className = "moure";
    } else if (tecla.keyCode == "54") {
        reproduirso("la")
        document.getElementById("la").className = "moure";
    } else if (tecla.keyCode == "55") {
        reproduirso("si")
        document.getElementById("si").className = "moure";
    } else if (tecla.keyCode == "56") {
        reproduirso("doa");
        document.getElementById("doa").className = "moure";
    }
}

//LLIBERACIO DE TECLA
function tornarTecla(tecla) {
    if (tecla.keyCode == "49") {
        reproduirso(1);
        document.getElementById("do").className = "pal";
    } else if (tecla.keyCode == "50") {
        reproduirso(2);
        document.getElementById("re").className = "pal";
    } else if (tecla.keyCode == "51") {
        reproduirso(3);
        document.getElementById("mi").className = "pal";
    } else if (tecla.keyCode == "52") {
        reproduirso(4);
        document.getElementById("fa").className = "pal";
    } else if (tecla.keyCode == "53") {
        reproduirso(5);
        document.getElementById("sol").className = "pal";
    } else if (tecla.keyCode == "54") {
        reproduirso(6);
        document.getElementById("la").className = "pal";
    } else if (tecla.keyCode == "55") {
        reproduirso(7);
        document.getElementById("si").className = "pal";
    } else if (tecla.keyCode == "56") {
        reproduirso(8);
        document.getElementById("doa").className = "pal";
    }
}

//ENSENYEM L'ARRAY EN PANTALLA AMB LA FUNCIO SETTIMEOUT.
function mostrarnota() {
    puntuaciocanviada = false;
    posicio++;
    var notaactual = array[posicio];

    document.getElementById("notestext").innerHTML = notaactual;

    if (posicio < array.length - 1) {
        bucle = setTimeout(mostrarnota, 2000);
    }
}

//S'EXECUTA AL APRETAR EL BOTO "COMENCAR", LES PRIMERES 4 LINEAS
//REINICIALITZEN VARIABLES I PARA EL BUCLE QUE ENSENYA L'ARRAY
//PER A COMENÇAR UNA NOVA CANÇO. AQUESTA FUNCIO CRIDA MOSTRAR NOTA
//QUE EN FUNCIO DE LA CANÇO TRIADA EN EL SELECT BUIDA L'ARRAY D'AQUELLA CANÇO
function start() {
    puntuacio = 0;
    document.getElementById("puntuacio").innerText = (puntuacio);
    clearTimeout(bucle);
    posicio = -1;
    var triat = document.getElementById("cancons").value;
    if (triat != 'tria') {
        comencar = true;
    }
    if (triat == 'gegant') {
        array = gegant;
        document.getElementById("nomcanco").innerText = "Gegant del Pi";
        document.getElementById("notestext").innerHTML = array[0];
        mostrarnota();

    } else if (triat == 'nadal') {
        array = nadal;
        document.getElementById("nomcanco").innerText = "Ara ve Nadal";
        document.getElementById("notestext").innerHTML = array[0];
        mostrarnota();
    } else {

    }
}

//FUNCIONS PER TRIAR SEGUENT I ANTERIOR OPCIONS DEL SELECT
function seguentcanco() {
    var canco;
    switch (document.getElementById("cancons").value) {
        case "tria":
            canco = document.getElementById("tria").nextElementSibling.value;
            break;
        case "gegant":
            canco = document.getElementById("gegant").nextElementSibling.value;
            break;
        case "nadal":
            canco = document.getElementById("nadal").nextElementSibling.value;
            break;
    }
    document.getElementById("cancons").value = canco;
}

function anteriorcanco() {
    var canco;
    switch (document.getElementById("cancons").value) {
        case "tria":
            canco = document.getElementById("tria").previousElementSibling.value;
            break;
        case "gegant":
            canco = document.getElementById("gegant").previousElementSibling.value;
            break;
        case "nadal":
            canco = document.getElementById("nadal").previousElementSibling.value;
            break;
    }
    document.getElementById("cancons").value = canco;
}

//FUNCTIONS QUE PUNTUEN
function puntuaciobe() {
	document.getElementById("notes").className = "notesbe";
    if (!puntuaciocanviada) {
		document.getElementById("puntuacio").innerText = (puntuacio = puntuacio + 50);
		puntuaciocanviada = true;                
	}
				
} 

function puntuaciomalament() {
	document.getElementById("notes").className = "notesmalament";
    if (posicio < array.length - 1) {
		document.getElementById("puntuacio").innerText = (puntuacio = puntuacio - 10);                
	}
} 
